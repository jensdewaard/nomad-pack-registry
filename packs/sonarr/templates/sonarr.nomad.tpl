job "sonarr" {
  datacenters = [ [[ range $idx, $dc := .sonarr.datacenters ]][[if $idx]],[[end]][[ $dc | quote ]][[ end ]] ]
  type = "service"

  group "sonarr" {
    count = [[ .sonarr.app_count ]]

    network {
      port "http" {
        to = [[ .sonarr.port.to | quote ]]
        [[- if gt (len .sonarr.port.static) 0 ]]
        static = [[ .sonarr.port.static | quote ]]
        [[- end ]]
        [[- if gt (len .sonarr.port.host_network) 0 ]]
        host_network = [[ .sonarr.port.host_network | quote ]]
        [[- end ]]
      }
    }

    service {
      port = "http"
      name = [[ .sonarr.service.name | quote ]]
      [[- if gt (len .sonarr.service.tags) 0 ]]
      tags = [
        [[- range $idx, $tag := .sonarr.service.tags ]]
        [[ $tag | quote ]],
        [[- end ]]
      ]
      [[- end ]]

      check {
        type = "http"
        port = "http"
        path = "/"
        interval = "60s"
        timeout = "5s"
      }
    }

    [[- range $idx, $mount := .sonarr.volumes ]]
    volume [[ $mount.name | quote ]] {
      type = [[ $mount.type | quote ]]
      source = [[ $mount.source | quote ]]
    }
    [[- end ]]

    task "sonarr" {
      driver = "docker"

      config {
        image = "[[ .sonarr.image ]]:[[ .sonarr.image_tag ]]"
        ports = ["http"]
        [[- if gt (len .sonarr.docker_network) 0 ]]
        network_mode = [[ .sonarr.docker_network | quote ]]
        [[- end ]]
        network_aliases = [
          "${NOMAD_TASK_NAME}",
        ]
      }

      resources {
        cpu = [[ .sonarr.resources.cpu ]]
        memory = [[ .sonarr.resources.memory ]]
      }

      env {
        TZ = [[ .sonarr.timezone | quote ]]
        PUID = [[ .sonarr.uid | quote ]]
        PGID = [[ .sonarr.gid | quote ]]
      }

      [[- range $idx, $mount := .sonarr.volumes ]]
      volume_mount {
        volume = [[ $mount.name | quote ]]
        destination = [[ $mount.destination | quote ]]
      }

      [[- end ]]
    }
  }
}
