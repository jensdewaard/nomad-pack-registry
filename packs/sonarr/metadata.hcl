app {
  url = "https://sonarr.tv"
  author = "Jens de Waard"
}

pack {
  name = "sonarr"
  description = "A PVR for Usenet and torrents"
  url = "https://gitlab.com/jensdewaard/nomad-pack-registry/sonarr"
  version = "0.0.1"
}