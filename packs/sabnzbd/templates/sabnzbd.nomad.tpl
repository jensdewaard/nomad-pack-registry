job "sabnzbd" {
  datacenters = [ [[ range $idx, $dc := .sabnzbd.datacenters ]][[if $idx]],[[end]][[ $dc | quote ]][[ end ]] ]
  type = "service"

  group "sabnzbd" {
    count = [[ .sabnzbd.app_count ]]

    network {
      port "http" {
        to = "8080"
        [[- if gt (len .sabnzbd.http_port_host_network) 0 ]]
        host_network = [[ .sabnzbd.http_port_host_network | quote ]]
        [[- end ]]
      }
    }

    service {
      port = "http"
      name = [[ .sabnzbd.service.name | quote ]]
      [[- if gt (len .sabnzbd.service.tags) 0 ]]
      tags = [
        [[- range $idx, $tag := .sabnzbd.service.tags ]]
        [[ $tag | quote ]],
        [[- end ]]
      ]
      [[- end ]]

      check {
        type = "http"
        port = "http"
        path = "/"
        interval = "60s"
        timeout = "5s"
      }
    }

    [[- range $idx, $mount := .sabnzbd.volumes ]]
    volume [[ $mount.name | quote ]] {
      type = [[ $mount.type | quote ]]
      source = [[ $mount.source | quote ]]
    }
    [[- end ]]

    task "sabnzbd" {
      driver = "docker"

      config {
        image = "[[ .sabnzbd.image ]]:[[ .sabnzbd.image_tag ]]"
        ports = ["http"]
        [[- if gt (len .sabnzbd.docker_network) 0 ]]
        network_mode = [[ .sabnzbd.docker_network | quote ]]
        [[- end ]]
        network_aliases = [
          "${NOMAD_TASK_NAME}",
        ]
      }

      resources {
        cpu = [[ .sabnzbd.resources.cpu ]]
        memory = [[ .sabnzbd.resources.memory ]]
      }

      env {
        TZ = [[ .sabnzbd.timezone | quote ]]
        PUID = [[ .sabnzbd.uid | quote ]]
        PGID = [[ .sabnzbd.gid | quote ]]
      }

      [[- range $idx, $mount := .sabnzbd.volumes ]]
      volume_mount {
        volume = [[ $mount.name | quote ]]
        destination = [[ $mount.destination | quote ]]
      }

      [[- end ]]
    }
  }
}
