app {
  url = "https://sabnzbd.org"
  author = "Jens de Waard"
}

pack {
  name = "sabnzbd"
  description = "The free and easy binary newsreader"
  url = "https://gitlab.com/jensdewaard/nomad-pack-registry/sabnzbd"
  version = "0.0.1"
}