variable "datacenters" {
  description = "A list of datacenters in the region which are eligible for task placement."
  type        = list(string)
  default     = ["dc1"]
}

variable "resources" {
  description = "The resources to assign to the application."
  type = object({
    cpu    = number
    memory = number
  })
  default = {
    cpu    = 1000,
    memory = 10240,
  }
}

variable "timezone" {
  description = "The timezone to assign to the application"
  type = string
  default = "Europe/Amsterdam"
}

variable "uid" {
  description = "The user ID to run the application under"
  type = string
  default = "1000"
}

variable "gid" {
  description = "The group ID to run the application under"
  type = string
  default = "1000"
}

variable "volumes" {
  description = "The volumes to mount in the container"
  type = list(object({
    name = string
    type = string
    source = string
    destination = string
  }))
}

variable "service" {
  description = "configuration for the nomad service"
  type = object({
    name = string
    tags = list(string)
  })
  default = {
    name = "satisfactory"
    tags = []
  }
}

variable "image" {
  description = "the docker image to run sonarr with"
  type = string
  default = "wolveix/satisfactory-server"
}

variable "image_tag" {
  description = "the image tag to run"
  type = string
  default = "latest"
}

variable "docker_network" {
  description = "A docker network to attach to"
  type = string
  default = ""
}

variable "port" {
  description = "configuration for the network port binding"
  type = object({
    to = string
    static = string
    host_network = string
  })
  default = {
    to = "8989"
    static = ""
    host_network = ""
  }
}

variable "autopause" {
  description = "pause game when no player is connected"
  type = bool
  default = true
}

variable "autosave_interval" {
  description = "autosave interval in seconds"
  type = number
  default = 300
}

variable "autosave_num" {
  description = "number of rotating autosave files"
  type = number
  default = 3
}

variable "autosave_on_disconnect" {
  description = "autosave when last player disconnects"
  type = bool
  default = true
}

variable "crash_report" {
  description = "automatic crash reporting"
  type = bool
  default = true
}

variable "debug" {
  description = "for debugging the server"
  type = bool
  default = false
}

variable "disable_seasonal_events" {
  description = "disable the FICSMAS event"
  type = bool
  default = false
}

variable "max_players" {
  description = "set the player limit for your server"
  type = number
  default = 4
}

variable "network_quality" {
  description = "set the network quality/bandwidth for your server"
  type = number
  default = 3
}

variable "server_beacon_port" {
  description = "set the game's beacon port"
  type = number
  default = 15000
}

variable "server_beacon_host_network" {
  description = "the host network the beacon port should listen on"
  type = string
  default = ""
}

variable "server_game_port" {
  description = "set the game's port"
  type = number
  default = 7777
}

variable "server_game_host_network" {
  description = "the host network the game port should listen on"
  type = string
  default = ""
}

variable "server_ip" {
  description = "set the game's ip"
  type = string
  default = "0.0.0.0"
}

variable "server_query_port" {
  description = "set the game's query port"
  type = number
  default = 15777
}

variable "server_query_host_network" {
  description = "the host network the server query port should listen on"
  type = string
  default = ""
}

variable "update_on_start" {
  description = "update the game on container startup"
  type = bool
  default = true
}

variable "steam_beta" {
  description = "use the experimental game version"
  type = bool
  default = false
}