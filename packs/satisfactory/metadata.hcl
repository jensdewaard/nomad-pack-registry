app {
  url = "https://satisfactorygame.com"
  author = "Jens de Waard"
}

pack {
  name = "satisfactory"
  description = "Satisfactory is a first-person open-world factory building game with a dash of exploration and combat."
  url = "https://gitlab.com/jensdewaard/nomad-pack-registry/satisfactory"
  version = "0.0.2"
}