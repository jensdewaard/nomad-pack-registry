job "satisfactory" {
  datacenters = [ [[ range $idx, $dc := .satisfactory.datacenters ]][[if $idx]],[[end]][[ $dc | quote ]][[ end ]] ]
  type = "service"

  group "satisfactory" {
    network {
      port "game" {
        to = [[ .satisfactory.server_game_port | quote ]]
        static = [[ .satisfactory.server_game_port | quote ]]
        [[- if gt (len .satisfactory.server_game_host_network) 0 ]]
        host_network = [[ .satisfactory.server_game_host_network | quote ]]
        [[- end ]]
      }

      port "beacon" {
        to = [[ .satisfactory.server_beacon_port | quote ]]
        static = [[ .satisfactory.server_beacon_port | quote ]]
        [[- if gt (len .satisfactory.server_beacon_host_network) 0 ]]
        host_network = [[ .satisfactory.server_beacon_host_network | quote ]]
        [[- end ]]
      }

      port "query" {
        to = [[ .satisfactory.server_query_port | quote ]]
        static = [[ .satisfactory.server_query_port | quote ]]
        [[- if gt (len .satisfactory.server_query_host_network) 0 ]]
        host_network = [[ .satisfactory.server_query_host_network | quote ]]
        [[- end ]]
      }
    }

    [[- range $idx, $mount := .satisfactory.volumes ]]
    volume [[ $mount.name | quote ]] {
      type = [[ $mount.type | quote ]]
      source = [[ $mount.source | quote ]]
    }
    [[- end ]]

    task "satisfactory" {
      driver = "docker"
      config {
        image = "[[ .satisfactory.image ]]:[[ .satisfactory.image_tag ]]"
        ports = [ "game", "beacon", "query" ]

        [[- if gt (len .satisfactory.docker_network) 0 ]]
        network_mode = [[ .satisfactory.docker_network | quote ]]
        [[- end ]]
      }

      resources {
        cpu = [[ .satisfactory.resources.cpu ]]
        memory = [[ .satisfactory.resources.memory ]]
      }

      env {
        AUTOPAUSE = [[ .satisfactory.autopause | quote ]]
        AUTOSAVEINTERVAL = [[ .satisfactory.autosave_interval | quote ]]
        AUTOSAVENUM = [[ .satisfactory.autosave_num | quote ]]
        AUTOSAVEONDISCONNECT = [[ .satisfactory.autosave_on_disconnect | quote ]]
        CRASHREPORT = [[ .satisfactory.crash_report | quote ]]
        DEBUG = [[ .satisfactory.debug | quote ]]
        DISABLESEASONALEVENTS = [[ .satisfactory.disable_seasonal_events | quote ]]
        MAXPLAYERS = [[ .satisfactory.max_players | quote ]]
        NETWORKQUALITY = [[ .satisfactory.network_quality | quote ]]
        PUID = [[ .satisfactory.uid | quote ]]
        PGID = [[ .satisfactory.gid | quote ]]
        SERVERBEACONPORT = [[ .satisfactory.server_beacon_port | quote ]]
        SERVERGAMEPORT = [[ .satisfactory.server_game_port | quote ]]
        SERVERIP = [[ .satisfactory.server_ip | quote ]]
        SERVERQUERYPORT = [[ .satisfactory.server_query_port | quote ]]
        SKIPUPDATE = [[ not .satisfactory.update_on_start | quote ]]
        STEAMBETA = [[ .satisfactory.steam_beta | quote ]]
      }

      [[- range $idx, $mount := .satisfactory.volumes ]]
      volume_mount {
        volume = [[ $mount.name | quote ]]
        destination = [[ $mount.destination | quote ]]
      }
      [[- end ]]
    }
  }
}