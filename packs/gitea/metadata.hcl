app {
  url = "https://gitea.io"
  author = "Jens de Waard"
}

pack {
  name = "gitea"
  description = "A painless self-hosted Git service."
  url = "https://gitlab.com/jensdewaard/nomad-pack-registry/gitea"
  version = "0.0.1"
}