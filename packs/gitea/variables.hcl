variable "datacenters" {
  description = "A list of datacenters in the region which are eligible for task placement."
  type        = list(string)
  default     = ["dc1"]
}

variable "app_count" {
  description = "The number of instances to deploy"
  type        = number
  default     = 1
}

variable "resources" {
  description = "The resources to assign to the application."
  type = object({
    cpu    = number
    memory = number
  })
  default = {
    cpu    = 100,
    memory = 300,
  }
}

variable "timezone" {
  description = "The timezone to assign to the application"
  type = string
  default = "Europe/Amsterdam"
}

variable "uid" {
  description = "The user ID to run the application under"
  type = string
  default = "1000"
}

variable "gid" {
  description = "The group ID to run the application under"
  type = string
  default = "1000"
}

variable "volumes" {
  description = "The volumes to mount in the container"
  type = list(object({
    name = string
    type = string
    source = string
    destination = string
  }))
}

variable "service" {
  description = "configuration for the nomad service"
  type = object({
    name = string
    tags = list(string)
  })
  default = {
    name = "gitea"
    tags = []
  }
}

variable "gitea_image" {
  description = "the docker image to run sonarr with"
  type = string
  default = "gitea/gitea"
}

variable "gitea_tag" {
  description = "the image tag to run"
  type = string
  default = "latest"
}

variable "docker_network" {
  description = "A docker network to attach to"
  type = string
  default = ""
}

variable "ssh_port_static" {
  description = "whether the SSH port should be static"
  type = bool
  default = false
}

variable "ssh_domain" {
  description = "the domain to use in SSH clone templates."
  type = string
  default = "localhost"
}

variable "ssh_port" {
  description = "the port to bind the SSH service on, if static. Will also be used in clone templates."
  type = number
  default = 22
}

variable "ssh_port_network" {
  description = "the host network the SSH port should bind on"
  type = string
  default = ""
}

variable "web_port_static" {
  description = "whether the web port should be static"
  type = bool
  default = false
}

variable "web_port" {
  description = "the port to bind the web server on, if stati"
  type = string
  default = ""
}

variable "web_port_network" {
  description = "the host network to web port should bind on"
  type = string
  default = ""
}

variable "database_type" {
  description = "the type of database to use"
  type = string
  default = "postgres"
}

variable "database_host" {
  description = "the host of the database"
  type = string
  default = "localhost"
}

variable "database_port" {
  description = "the port of the database"
  type = number
  default = "5432"
}

variable "database_name" {
  description = "the name of the database"
  type = string
  default = "gitea"
}

variable "database_user" {
  description = "the user to connect to the database with"
  type = string
  default = "gitea"
}

variable "database_password" {
  description = "the password to authenticate to the database with"
  type = string
  default = "gitea"
}

variable "disable_registration" {
  description = "Disable registration, after which only admin can create accounts for users."
  type = bool
  default = false
}

