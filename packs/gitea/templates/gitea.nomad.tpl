job "gitea" {
  datacenters = [ [[ range $idx, $dc := .gitea.datacenters ]][[if $idx]],[[end]][[ $dc | quote ]][[ end ]] ]
  type = "service"

  group "gitea" {
    count = [[ .gitea.app_count ]]

    network {

      port "ssh" {
        to = "22"
        [[- if .gitea.ssh_port_static ]]
        static = [[- .gitea.ssh_port | quote ]]
        [[- end ]]
        [[- if gt (len .gitea.ssh_port_network) 0 ]]
        host_network = [[ .gitea.ssh_port_network | quote ]]
        [[- end ]]
      }

      port "http" {
        to = "3000"
        [[- if .gitea.web_port_static ]]
        static = [[- .gitea.web_port | quote ]]
        [[- end ]]
        [[- if gt (len .gitea.web_port_network) 0 ]]
        host_network = [[ .gitea.web_port_network | quote ]]
        [[- end ]]
      }
    }

    service {
      port = "http"
      name = [[ .gitea.service.name | quote ]]
      [[- if gt (len .gitea.service.tags) 0 ]]
      tags = [
        [[- range $idx, $tag := .gitea.service.tags ]]
        [[ $tag | quote ]],
        [[- end ]]
      ]
      [[- end ]]

      check {
        type = "http"
        port = "http"
        path = "/"
        interval = "60s"
        timeout = "5s"
      }
    }

    [[- range $idx, $mount := .gitea.volumes ]]
    volume [[ $mount.name | quote ]] {
      type = [[ $mount.type | quote ]]
      source = [[ $mount.source | quote ]]
    }
    [[- end ]]

    task "gitea" {
      driver = "docker"

      config {
        image = "[[ .gitea.gitea_image ]]:[[ .gitea.gitea_tag ]]"
        ports = ["http", "ssh"]
        [[- if gt (len .gitea.docker_network) 0 ]]
        network_mode = [[ .gitea.docker_network | quote ]]
        network_aliases = [
          "${NOMAD_TASK_NAME}"
        ]
        [[- end ]]
      }

      resources {
        cpu = [[ .gitea.resources.cpu ]]
        memory = [[ .gitea.resources.memory ]]
      }

      env {
        USER_UID=[[ .gitea.uid | quote ]]
        USER_GID=[[ .gitea.gid | quote ]]
        GITEA__database__DB_TYPE=[[ .gitea.database_type | quote ]]
        GITEA__database__DB_HOST="[[ .gitea.database_host ]]:[[ .gitea.database_port ]]"
        GITEA__database__DB_NAME=[[ .gitea.database_name | quote ]]
        GITEA__database__DB_USER=[[ .gitea.database_user | quote ]]
        GITEA__database__DB_PASSWD=[[ .gitea.database_password | quote ]]
        DISABLE_REGISTRATION=[[ .gitea.disable_registration | quote ]]
        SSH_DOMAIN=[[.gitea.ssh_domain | quote ]]
        SSH_PORT=[[ .gitea.ssh_port | quote ]]
      }

      [[- range $idx, $mount := .gitea.volumes ]]
      volume_mount {
        volume = [[ $mount.name | quote ]]
        destination = [[ $mount.destination | quote ]]
      }
      [[- end ]]
    }
  }
}
