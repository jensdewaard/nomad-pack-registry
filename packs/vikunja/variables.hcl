variable "datacenters" {
  description = "A list of datacenters in the region which are eligible for task placement."
  type        = list(string)
  default     = ["dc1"]
}

variable "resources" {
  description = "The resources to assign to the application."
  type = object({
    cpu    = number
    memory = number
  })
  default = {
    cpu    = 100,
    memory = 300,
  }
}

variable "timezone" {
  description = "The timezone to assign to the application"
  type = string
  default = "Europe/Amsterdam"
}

variable "uid" {
  description = "The user ID to run the application under"
  type = string
  default = "0"
}

variable "gid" {
  description = "The group ID to run the application under"
  type = string
  default = "0"
}

variable "volumes" {
  description = "The volumes to mount in the container"
  type = list(object({
    name = string
    type = string
    source = string
    destination = string
  }))
}

variable "service_name" {
  description = "the name of the service"
  type = string
  default = "vikunja"
}

variable "frontend_service_tags" {
  description = "tags for the service"
  type = list(string)
  default = []
}

variable "api_service_tags" {
  description = "tags for the service"
  type = list(string)
  default = []
}

variable "api_image" {
  description = "the docker image to run the Vikunja API with"
  type = string
  default = "vikunja/api"
}

variable "api_tag" {
  description = "the image tag for the API image"
  type = string
  default = "latest"
}

variable "frontend_image" {
  description = "the docker image to run Vikunja frontend with"
  type = string
  default = "vikunja/frontend"
}

variable "frontend_tag" {
  description = "the image tag for the API image"
  type = string
  default = "latest"
}

variable "docker_network" {
  description = "A docker network to attach to"
  type = string
  default = "vikunja"
}

variable "http_port" {
  description = "the port the frontend should listen on"
  type = number
  default = 80
}

variable "http_port_static" {
  description = "the static port the frontend should listen on, if any"
  type = string
  default = ""
}

variable "http_port_host_network" {
  description = "the host network for the http port to bind to"
  type = string
  default = ""
}

variable "api_port" {
  description = "the port the API should listen on"
  type = number
  default = 3456
}

variable "api_port_static" {
  description = "the static port the API should listen on, if any"
  type = string
  default = ""
}

variable "api_port_host_network" {
  description = "the host network for the http port to bind to"
  type = string
  default = ""
}

variable "database_type" {
  description = "the type of database to use"
  type = string
  default = "postgres"
}

variable "database_host" {
  description = "the host of the database"
  type = string
  default = "localhost"
}

variable "database_port" {
  description = "the port of the database"
  type = number
  default = "5432"
}

variable "database_name" {
  description = "the name of the database"
  type = string
  default = "vikunja"
}

variable "database_user" {
  description = "the user to connect to the database with"
  type = string
  default = "vikunja"
}

variable "database_password" {
  description = "the password to authenticate to the database with"
  type = string
  default = "vikunja"
}

variable "enable_registrations" {
  description = "enable user registrations"
  type = bool
  default = false
}