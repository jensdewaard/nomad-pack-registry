app {
  url = "https://vikunja.io"
  author = "Jens de Waard"
}

pack {
  name = "vikunja"
  description = " The open-source, self-hostable to-do app."
  url = "https://gitlab.com/jensdewaard/nomad-pack-registry/vikunja"
  version = "0.0.1"
}