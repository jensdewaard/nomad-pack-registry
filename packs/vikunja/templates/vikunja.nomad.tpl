job "vikunja" {
  datacenters = [ [[ range $idx, $dc := .vikunja.datacenters ]][[if $idx]],[[end]][[ $dc | quote ]][[ end ]] ]
  type = "service"

  group "vikunja" {
    network {
      port "http" {
        to = [[ .vikunja.http_port | quote ]]
        [[- if gt (len .vikunja.http_port_static) 0 ]]
        static = [[ .vikunja.http_port_static | quote ]]
        [[- end ]]
        [[- if gt (len .vikunja.http_port_host_network) 0 ]]
        host_network = [[ .vikunja.http_port_host_network | quote ]]
        [[- end ]]
      }

      port "api" {
        to = [[ .vikunja.api_port | quote ]]
        [[- if gt (len .vikunja.api_port_static) 0 ]]
        static = [[ .vikunja.api_port_static | quote ]]
        [[- end ]]
        [[- if gt (len .vikunja.api_port_host_network) 0 ]]
        host_network = [[ .vikunja.api_port_host_network | quote ]]
        [[- end ]]
      }
    }

    service {
      port = "http"
      name = [[ .vikunja.service_name | quote ]]
      [[- if gt (len .vikunja.frontend_service_tags) 0 ]]
      tags = [
        [[- range $idx, $tag := .vikunja.frontend_service_tags ]]
        [[ $tag | quote ]],
        [[- end ]]
      ]
      [[- end ]]

      check {
        type = "http"
        port = "http"
        path = "/"
        interval = "60s"
        timeout = "5s"
      }
    }

    service {
      port = "api"
      name = "vikunja-api"
      [[- if gt (len .vikunja.api_service_tags) 0 ]]
      tags = [
        [[- range $idx, $tag := .vikunja.api_service_tags ]]
        [[ $tag | quote ]],
        [[- end ]]
      ]
      [[- end ]]

      check {
        type = "http"
        port = "http"
        path = "/"
        interval = "60s"
        timeout = "5s"
      }
    }

    [[- range $idx, $mount := .vikunja.volumes ]]
    volume [[ $mount.name | quote ]] {
      type = [[ $mount.type | quote ]]
      source = [[ $mount.source | quote ]]
    }
    [[- end ]]

    task "vikunja-frontend" {
      driver = "docker"

      config {
        image = "[[ .vikunja.frontend_image ]]:[[ .vikunja.frontend_tag ]]"
        ports = ["http"]
        [[- if gt (len .vikunja.docker_network) 0 ]]
        network_mode = [[ .vikunja.docker_network | quote ]]
        network_aliases = [
          "${NOMAD_TASK_NAME}",
        ]
        [[- end ]]
      }


    }

    task "vikunja-api" {
      driver = "docker"

      config {
        image = "[[ .vikunja.api_image ]]:[[ .vikunja.api_tag ]]"
        ports = ["api"]
        [[- if gt (len .vikunja.docker_network) 0 ]]
        network_mode = [[ .vikunja.docker_network | quote ]]
        network_aliases = [
          "${NOMAD_TASK_NAME}",
        ]
        [[- end ]]
      }

      resources {
        cpu = [[ .vikunja.resources.cpu ]]
        memory = [[ .vikunja.resources.memory ]]
      }

      env {
        TZ = [[ .vikunja.timezone | quote ]]
        PUID = [[ .vikunja.uid | quote ]]
        PGID = [[ .vikunja.gid | quote ]]
        VIKUNJA_DATABASE_TYPE = [[ .vikunja.database_type | quote ]]
        VIKUNJA_DATABASE_HOST = "[[ .vikunja.database_host ]]:[[ .vikunja.database_port ]]"
        VIKUNJA_DATABASE_USER = [[ .vikunja.database_user | quote ]]
        VIKUNJA_DATABASE_PASSWORD = [[ .vikunja.database_password | quote ]]
        VIKUNJA_SERVICE_ENABLEREGISTRATION = [[ .vikunja.enable_registrations | quote ]]
      }
    }
  }
}
