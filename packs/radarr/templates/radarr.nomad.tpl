job "radarr" {
  datacenters = [ [[ range $idx, $dc := .radarr.datacenters ]][[if $idx]],[[end]][[ $dc | quote ]][[ end ]] ]
  type = "service"

  group "radarr" {
    count = [[ .radarr.app_count ]]

    network {
      port "http" {
        to = [[ .radarr.port.to | quote ]]
        [[- if gt (len .radarr.port.static) 0 ]]
        static = [[ .radarr.port.static | quote ]]
        [[- end ]]
        [[- if gt (len .radarr.port.host_network) 0 ]]
        host_network = [[ .radarr.port.host_network | quote ]]
        [[- end ]]
      }
    }

    service {
      port = "http"
      name = [[ .radarr.service.name | quote ]]
      [[- if gt (len .radarr.service.tags) 0 ]]
      tags = [
        [[- range $idx, $tag := .radarr.service.tags ]]
        [[ $tag | quote ]],
        [[- end ]]
      ]
      [[- end ]]

      check {
        type = "http"
        port = "http"
        path = "/"
        interval = "60s"
        timeout = "5s"
      }
    }

    [[- range $idx, $mount := .radarr.volumes ]]
    volume [[ $mount.name | quote ]] {
      type = [[ $mount.type | quote ]]
      source = [[ $mount.source | quote ]]
    }
    [[- end ]]

    task "radarr" {
      driver = "docker"

      config {
        image = "[[ .radarr.image ]]:[[ .radarr.image_tag ]]"
        ports = ["http"]
        [[- if gt (len .radarr.docker_network) 0 ]]
        network_mode = [[ .radarr.docker_network | quote ]]
        [[- end ]]
        network_aliases = [
          "${NOMAD_TASK_NAME}",
        ]
      }

      resources {
        cpu = [[ .radarr.resources.cpu ]]
        memory = [[ .radarr.resources.memory ]]
      }

      env {
        TZ = [[ .radarr.timezone | quote ]]
        PUID = [[ .radarr.uid | quote ]]
        PGID = [[ .radarr.gid | quote ]]
      }

      [[- range $idx, $mount := .radarr.volumes ]]
      volume_mount {
        volume = [[ $mount.name | quote ]]
        destination = [[ $mount.destination | quote ]]
      }

      [[- end ]]
    }
  }
}
