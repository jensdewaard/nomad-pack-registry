service = {
  name = "radarr"
  tags = [
    "traefik.http.routers.radarr.rule=Host(`radarr.jensdewaard.nl`)",
    "traefik.http.routers.radarr.tls=true",
    "traefik.http.routers.radarr.tls.certresolver=myresolver",
    "traefik.http.routers.radarr.middlewares=authelia",
  ]
}

image_tag = "4.1.0"
docker_network = "sonarr_stack"

port = {
  to = "7878"
  static = "7878"
  host_network = "zt"
}

volumes = [
  { name = "movies", type = "host", source = "movies", destination = "/movies" },
  { name = "radarr_config", type = "host", source = "radarr_config", destination = "/config" },
  { name = "downloads", type = "host", source = "downloads", destination = "/downloads" },
  { name = "usenet_downloads", type = "host", source = "usenet_downloads", destination = "/usenet_downloads" }
]