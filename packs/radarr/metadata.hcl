app {
  url = "https://radarr.video"
  author = "Jens de Waard"
}

pack {
  name = "radarr"
  description = "A movie collection manager for Usenet and Bittorrent"
  url = "https://gitlab.com/jensdewaard/nomad-pack-registry/radarr"
  version = "0.0.1"
}