variable "datacenters" {
  description = "A list of datacenters in the region which are eligible for task placement."
  type        = list(string)
  default     = ["dc1"]
}

variable "app_count" {
  description = "The number of instances to deploy"
  type        = number
  default     = 1
}

variable "resources" {
  description = "The resources to assign to the application."
  type = object({
    cpu    = number
    memory = number
  })
  default = {
    cpu    = 100,
    memory = 300,
  }
}

variable "timezone" {
  description = "The timezone to assign to the application"
  type = string
  default = "Europe/Amsterdam"
}

variable "uid" {
  description = "The user ID to run the application under"
  type = string
  default = "0"
}

variable "gid" {
  description = "The group ID to run the application under"
  type = string
  default = "0"
}

variable "volumes" {
  description = "The volumes to mount in the container"
  type = list(object({
    name = string
    type = string
    source = string
    destination = string
  }))
}

variable "service" {
  description = "configuration for the nomad service"
  type = object({
    name = string
    tags = list(string)
  })
  default = {
    name = "radarr"
    tags = []
  }
}

variable "image" {
  description = "the docker image to run sonarr with"
  type = string
  default = "linuxserver/radarr"
}

variable "image_tag" {
  description = "the image tag to run"
  type = string
  default = "latest"
}

variable "docker_network" {
  description = "A docker network to attach to"
  type = string
  default = ""
}

variable "port" {
  description = "configuration for the network port binding"
  type = object({
    to = string
    static = string
    host_network = string
  })
  default = {
    to = "7878"
    static = ""
    host_network = ""
  }
}