variable "datacenters" {
  description = "A list of datacenters in the region which are eligible for task placement."
  type        = list(string)
  default     = ["dc1"]
}

variable "app_count" {
  description = "The number of instances to deploy"
  type        = number
  default     = 1
}

variable "resources" {
  description = "The resources to assign to the application."
  type = object({
    cpu    = number
    memory = number
  })
  default = {
    cpu    = 100,
    memory = 300,
  }
}

variable "timezone" {
  description = "The timezone to assign to the application"
  type = string
  default = "Europe/Amsterdam"
}

variable "uid" {
  description = "The user ID to run the application under"
  type = string
  default = "0"
}

variable "gid" {
  description = "The group ID to run the application under"
  type = string
  default = "0"
}

variable "volumes" {
  description = "The volumes to mount in the container"
  type = list(object({
    name = string
    type = string
    source = string
    destination = string
  }))
}

variable "service_name" {
  description = "name of the nomad service"
  type = string
  default = "bazarr"
}

variable "service_tags" {
  description = "tags for the nomad service"
  type = list(string)
  default = []
}

variable "image" {
  description = "the docker image to run bazarr with"
  type = string
  default = "lscr.io/linuxserver/bazarr"
}

variable "tag" {
  description = "the image tag to run"
  type = string
  default = "latest"
}

variable "host_network" {
  description = "a host network to attach the port on"
  type = string
  default = ""
}

variable "docker_network" {
  description = "A docker network to attach to"
  type = string
  default = ""
}