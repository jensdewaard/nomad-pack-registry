app {
  url = "https://www.bazarr.media"
  author = "Jens de Waard"
}

pack {
  name = "bazarr"
  description = "Bazarr is a companion application to Sonarr and Radarr that manages and downloads subtitles based on your requirements."
  url = "https://gitlab.com/jensdewaard/nomad-pack-registry/bazarr"
  version = "0.0.1"
}