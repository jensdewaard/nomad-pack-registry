job "bazarr" {
  datacenters = [ [[ range $idx, $dc := .bazarr.datacenters ]][[if $idx]],[[end]][[ $dc | quote ]][[ end ]] ]
  type = "service"

  group "bazarr" {
    count = [[ .bazarr.app_count ]]

    network {
      port "http" {
        to = "6767"
        [[- if gt (len .bazarr.host_network) 0 ]]
        host_network = [[ .bazarr.host_network | quote ]]
        [[- end ]]
      }
    }

    service {
      port = "http"
      name = [[ .bazarr.service_name | quote ]]

      [[- if gt (len .bazarr.service_tags) 0 ]]
      tags = [
        [[- range $idx, $tag := .bazarr.service_tags ]]
        [[ $tag | quote ]],
        [[- end ]]
      ]
      [[- end ]]
    }

    [[- range $idx, $mount := .bazarr.volumes ]]
    volume [[ $mount.name | quote ]] {
      type = [[ $mount.type | quote ]]
      source = [[ $mount.source | quote ]]
    }
    [[- end ]]

    task "bazarr" {
      driver = "docker"

      config {
        image = "[[ .bazarr.image ]]:[[ .bazarr.tag ]]"
        ports = ["http"]
        [[- if gt (len .bazarr.docker_network) 0 ]]
        network_mode = [[ .bazarr.docker_network | quote ]]
        network_aliases = [
          "${NOMAD_TASK_NAME}",
        ]
        [[- end ]]
      }

      resources {
        cpu = [[ .bazarr.resources.cpu ]]
        memory = [[ .bazarr.resources.memory ]]
      }

      env {
        TZ = [[ .bazarr.timezone | quote ]]
        PUID = [[ .bazarr.uid | quote ]]
        PGID = [[ .bazarr.gid | quote ]]
      }

      [[- range $idx, $mount := .bazarr.volumes ]]
      volume_mount {
        volume = [[ $mount.name | quote ]]
        destination = [[ $mount.destination | quote ]]
      }
      [[- end ]]
    } # task
  } # group
}