Congrats on deploying [[ .nomad_pack.pack.name ]].

There are [[ .firefly_iii.app_count ]] instances of your job now running on Nomad.
