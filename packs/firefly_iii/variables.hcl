variable "datacenters" {
  description = "A list of datacenters in the region which are eligible for task placement."
  type        = list(string)
  default     = ["dc1"]
}

variable "app_count" {
  description = "The number of instances to deploy"
  type        = number
  default     = 1
}

variable "resources" {
  description = "The resources to assign to the application."
  type = object({
    cpu    = number
    memory = number
  })
  default = {
    cpu    = 100,
    memory = 300,
  }
}

variable "timezone" {
  description = "The timezone to assign to the application"
  type = string
  default = "Europe/Amsterdam"
}

variable "uid" {
  description = "The user ID to run the application under"
  type = string
  default = "1000"
}

variable "gid" {
  description = "The group ID to run the application under"
  type = string
  default = "1000"
}

variable "service_name" {
  description = "configuration for the nomad service"
  type = string
  default = "firefly"
}

variable "service_tags" {
  description = "tags for the service"
  type = list(string)
  default = []
}

variable "image" {
  description = "the docker image to run firefly with"
  type = string
  default = "fireflyiii/core"
}

variable "tag" {
  description = "the image tag to run"
  type = string
  default = "latest"
}

variable "host_network" {
  description = "the host network to port should bind on"
  type = string
  default = ""
}

variable "database_type" {
  description = "the type of database to use"
  type = string
  default = "pgsql"
}

variable "database_host" {
  description = "the host of the database"
  type = string
  default = "localhost"
}

variable "database_port" {
  description = "the port of the database"
  type = number
  default = "5432"
}

variable "database_name" {
  description = "the name of the database"
  type = string
  default = "firefly"
}

variable "database_user" {
  description = "the user to connect to the database with"
  type = string
  default = "firefly"
}

variable "database_password" {
  description = "the password to authenticate to the database with"
  type = string
  default = "firefly"
}

variable "trusted_proxies" {
  description = ""
  type = string
  default = "**"
}

variable "app_key" {
  description = ""
  type = string
}

variable "app_url" {
  description = ""
  type = string
}

variable "site_owner" {
  description = ""
  type = string
}