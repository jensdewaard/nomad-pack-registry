app {
  url = "https://firefly-iii.org"
  author = "Jens de Waard"
}

pack {
  name = "firefly_iii"
  description = "A free and open source personal finance manager."
  url = "https://gitlab.com/jensdewaard/nomad-pack-registry/firefly-iii"
  version = "0.0.1"
}