job "firefly" {
  datacenters = [ [[ range $idx, $dc := .firefly_iii.datacenters ]][[if $idx]],[[end]][[ $dc | quote ]][[ end ]] ]
  type = "service"

  group "firefly" {
    network {
      port "http" {
        to = "8080"
        [[- if gt (len .firefly_iii.host_network) 0 ]]
        host_network = [[ .firefly_iii.host_network | quote ]]
        [[- end ]]
      }
    }

    service {
      port = "http"
      name = [[ .firefly_iii.service_name | quote ]]
      [[- if gt (len .firefly_iii.service_tags) 0 ]]
      tags = [
        [[- range $idx, $tag := .firefly_iii.service_tags ]]
        [[ $tag | quote ]],
        [[- end ]]
      ]
      [[- end ]]
    }

    task "firefly" {
      driver = "docker"

      config {
        image = "[[ .firefly_iii.image ]]:[[ .firefly_iii.tag ]]"
        ports = ["http"]
      }

      resources {
        cpu = [[ .firefly_iii.resources.cpu ]]
        memory = [[ .firefly_iii.resources.memory ]]
      }

      env {
        DB_HOST=[[ .firefly_iii.database_host | quote ]]
        DB_DATABASE=[[ .firefly_iii.database_name | quote ]]
        DB_USERNAME=[[ .firefly_iii.database_user | quote ]]
        DB_PORT=[[ .firefly_iii.database_port | quote ]]
        DB_CONNECTION=[[ .firefly_iii.database_type | quote ]]
        DB_PASSWORD=[[ .firefly_iii.database_password | quote ]]
        APP_KEY=[[ .firefly_iii.app_key | quote ]]
        APP_URL=[[ .firefly_iii.app_url | quote ]]
        TRUSTED_PROXIES=[[ .firefly_iii.trusted_proxies | quote ]]
        SITE_OWNER=[[ .firefly_iii.site_owner | quote ]]
      }
    }
  }
}
