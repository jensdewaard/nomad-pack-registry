job "drone" {
  datacenters = [ [[ range $idx, $dc := .drone.datacenters ]][[if $idx]],[[end]][[ $dc | quote ]][[ end ]] ]
  type = "service"

  group "drone" {
    network {
      port "http" {
        to = [[ .drone.http_port | quote ]]
        [[- if gt (len .drone.http_port_static) 0 ]]
        static = [[ .drone.http_port_static | quote ]]
        [[- end ]]
        [[- if gt (len .drone.http_port_host_network) 0 ]]
        host_network = [[ .drone.http_port_host_network | quote ]]
        [[- end ]]
      }

      port "runner" {
        to = 3000
      }
    }

    service {
      port = "http"
      name = [[ .drone.service_name | quote ]]
      [[- if gt (len .drone.service_tags) 0 ]]
      tags = [
        [[- range $idx, $tag := .drone.service_tags ]]
        [[ $tag | quote ]],
        [[- end ]]
      ]
      [[- end ]]

      check {
        type = "http"
        port = "http"
        path = "/"
        interval = "60s"
        timeout = "5s"
      }
    }

    [[- range $idx, $mount := .drone.volumes ]]
    volume [[ $mount.name | quote ]] {
      type = [[ $mount.type | quote ]]
      source = [[ $mount.source | quote ]]
    }
    [[- end ]]

    volume "docker" {
      type = "host"
      source = "docker-socket"
    }

    task "drone-server" {
      driver = "docker"

      config {
        image = "[[ .drone.image ]]:[[ .drone.tag ]]"
        ports = ["http"]
        [[- if gt (len .drone.docker_network) 0 ]]
        network_mode = [[ .drone.docker_network | quote ]]
        network_aliases = [
          "${NOMAD_TASK_NAME}",
        ]
        [[- end ]]
      }

      resources {
        cpu = [[ .drone.resources.cpu ]]
        memory = [[ .drone.resources.memory ]]
      }

      env {
        TZ = [[ .drone.timezone | quote ]]
        PUID = [[ .drone.uid | quote ]]
        PGID = [[ .drone.gid | quote ]]
        DRONE_GITEA_CLIENT_ID = [[ .drone.gitea_client_id | quote ]]
        DRONE_GITEA_CLIENT_SECRET = [[ .drone.gitea_client_secret | quote ]]
        DRONE_GITEA_SERVER = [[ .drone.gitea_server | quote ]]
        DRONE_RPC_SECRET = [[ .drone.rpc_secret | quote ]]
        DRONE_SERVER_HOST = [[ .drone.server_host | quote ]]
        DRONE_GIT_ALWAYS_AUTH="true"
        DRONE_SERVER_PROTO = [[ .drone.server_proto | quote ]]
      } # env

      [[- range $idx, $mount := .drone.volumes ]]
      volume_mount {
        volume = [[ $mount.name | quote ]]
        destination = [[ $mount.destination | quote ]]
      }
      [[- end ]]
    } # task

    task "drone-runner" {
      driver = "docker"

      config {
        image = "drone/drone-runner-docker:1"
        [[- if gt (len .drone.docker_network) 0 ]]
        network_mode = [[ .drone.docker_network | quote ]]
        network_aliases = [
          "${NOMAD_TASK_NAME}",
        ]
        [[- end ]]
      }

      resources {
        cpu = 1000
        memory = 1024
      }

      env {
        DRONE_RPC_PROTO="http"
        DRONE_RPC_HOST="drone-server"
        DRONE_RPC_SECRET=[[ .drone.rpc_secret | quote ]]
        DRONE_RUNNER_CAPACITY=1
        DRONE_RUNNER_NAME=runner-1
      }

      volume_mount {
        volume = "docker"
        destination = "/var/run/docker.sock"
      }
    } # task
  } # group
} # job
