app {
  url = "https://drone.io"
  author = "Jens de Waard"
}

pack {
  name = "drone"
  description = "Drone is a self-service Continuous Integration platform for busy development teams."
  url = "https://gitlab.com/jensdewaard/nomad-pack-registry/drone"
  version = "0.0.1"
}