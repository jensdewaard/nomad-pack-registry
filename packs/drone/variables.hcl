variable "datacenters" {
  description = "A list of datacenters in the region which are eligible for task placement."
  type        = list(string)
  default     = ["dc1"]
}

variable "resources" {
  description = "The resources to assign to the application."
  type = object({
    cpu    = number
    memory = number
  })
  default = {
    cpu    = 100,
    memory = 300,
  }
}

variable "timezone" {
  description = "The timezone to assign to the application"
  type = string
  default = "Europe/Amsterdam"
}

variable "uid" {
  description = "The user ID to run the application under"
  type = string
  default = "0"
}

variable "gid" {
  description = "The group ID to run the application under"
  type = string
  default = "0"
}

variable "volumes" {
  description = "The volumes to mount in the container"
  type = list(object({
    name = string
    type = string
    source = string
    destination = string
  }))
}

variable "service_name" {
  description = "the name of the service"
  type = string
  default = "drone"
}

variable "service_tags" {
  description = "tags for the service"
  type = list(string)
  default = []
}

variable "image" {
  description = "the docker image to run Drone with"
  type = string
  default = "drone/drone"
}

variable "tag" {
  description = "the image tag for the API image"
  type = string
  default = "latest"
}

variable "docker_network" {
  description = "A docker network to attach to"
  type = string
  default = "vikunja"
}

variable "http_port" {
  description = "the port the frontend should listen on"
  type = number
  default = 80
}

variable "http_port_static" {
  description = "the static port the frontend should listen on, if any"
  type = string
  default = ""
}

variable "http_port_host_network" {
  description = "the host network for the http port to bind to"
  type = string
  default = ""
}

variable "gitea_server" {
  description = "Required string value provides your Gitea server address."
  type = string
}

variable "gitea_client_id" {
  description = "Required string value provides your Gitea oauth Client ID."
  type = string
}

variable "gitea_client_secret" {
  description = "Required string value provides your Gitea oauth Client Secret."
  type = string
}

variable "rpc_secret" {
  description = "Required string value provides the shared secret for communicating between runners and server."
  type = string
}

variable "server_host" {
  description = "Required string value provides your external hostname or IP address."
  type = string
}

variable "server_proto" {
  description = "Required string value provides your external protocol scheme. This value should be set to http or https."
  type = string
  default = "http"
}