app {
  url = "https://grocy.info"
  author = "Jens de Waard"
}

pack {
  name = "grocy"
  description = "ERP Beyond The Fridge."
  url = "https://gitlab.com/jensdewaard/nomad-pack-registry/grocy"
  version = "0.0.1"
}