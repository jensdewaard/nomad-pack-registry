job "grocy" {
  datacenters = [ [[ range $idx, $dc := .grocy.datacenters ]][[if $idx]],[[end]][[ $dc | quote ]][[ end ]] ]
  type = "service"

  group "grocy" {
    network {
      port "http" {
        to = "80"
        [[- if gt (len .grocy.static_port) 0 ]]
        static = [[ .grocy.static_port | quote ]]
        [[- end ]]
        [[- if gt (len .grocy.host_network) 0 ]]
        host_network = [[ .grocy.host_network | quote ]]
        [[- end]]
      }
    }

    service {
      port = "http"
      name = "grocy"
      [[- if gt (len .grocy.service_tags) 0 ]]
      tags = [
        [[- range $idx, $tag := .grocy.service_tags ]]
        [[ $tag | quote ]],
        [[- end ]]
      ]
      [[- end ]]
    }

    [[- range $idx, $mount := .grocy.volumes ]]
    volume [[ $mount.name | quote ]] {
      type = [[ $mount.type | quote ]]
      source = [[ $mount.source | quote ]]
    }
    [[- end ]]

    task "grocy" {
      driver = "docker"

      config {
        image = "[[ .grocy.image ]]:[[ .grocy.tag ]]"
        ports = ["http"]
      }

      resources {
        cpu = [[ .grocy.resources.cpu ]]
        memory = [[ .grocy.resources.memory ]]
      }

      env {
        TZ="Europe/Amsterdam"
        GROCY_CURRENCY="EUR"
        GROCY_FEATURE_FLAG_CHORES="false"
        GROCY_FEATURE_FLAG_TASKS="false"
        GROCY_FEATURE_FLAG_BATTERIES="false"
        GROCY_FEATURE_FLAG_EQUIPMENT="false"
        GROCY_FEATURE_FLAG_STOCK_PRICING="false"
        GROCY_CALENDAR_FIRST_DAY_OF_WEEK=[[ .grocy.first_day_of_week | quote ]]
      }

      [[- range $idx, $mount := .grocy.volumes ]]
      volume_mount {
        volume = [[ $mount.name | quote ]]
        destination = [[ $mount.destination | quote ]]
      }
      [[- end ]]
    }
  }
}
