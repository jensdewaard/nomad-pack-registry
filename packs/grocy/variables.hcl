variable "datacenters" {
  description = "A list of datacenters in the region which are eligible for task placement."
  type        = list(string)
  default     = ["dc1"]
}

variable "app_count" {
  description = "The number of instances to deploy"
  type        = number
  default     = 1
}

variable "resources" {
  description = "The resources to assign to the application."
  type = object({
    cpu    = number
    memory = number
  })
  default = {
    cpu    = 100,
    memory = 300,
  }
}

variable "timezone" {
  description = "The timezone to assign to the application"
  type = string
  default = "Europe/Amsterdam"
}

variable "uid" {
  description = "The user ID to run the application under"
  type = string
  default = "1000"
}

variable "gid" {
  description = "The group ID to run the application under"
  type = string
  default = "1000"
}

variable "volumes" {
  description = "The volumes to mount in the container"
  type = list(object({
    name = string
    type = string
    source = string
    destination = string
  }))
}

variable "service_tags" {
  description = "configuration for the nomad service"
  type = list(string)
  default = []
}

variable "image" {
  description = "the docker image to run grocy with"
  type = string
  default = "lscr.io/linuxserver/grocy"
}

variable "tag" {
  description = "the image tag to run"
  type = string
  default = "latest"
}

variable "static_port" {
  description = "a port to statically bind to"
  type = string
  default = ""
}

variable "host_network" {
  description = "the host network the HTTP port should bind on"
  type = string
  default = ""
}

variable "first_day_of_week" {
  description = "the first day of the week"
  type = number
  default = 0
}
