app {
  url = "https://transmissionbt.com"
  author = "Jens de Waard"
}

pack {
  name = "transmission"
  description = "A Fast, Easy and Free Bittorrent Client"
  url = "https://gitlab.com/jensdewaard/nomad-pack-registry/transmission"
  version = "0.0.1"
}