Congrats on deploying [[ .nomad_pack.pack.name ]].

There are [[ .transmission.app_count ]] instances of your job now running on Nomad.
