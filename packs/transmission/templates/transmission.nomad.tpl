job "transmission" {
  datacenters = [ [[ range $idx, $dc := .transmission.datacenters ]][[if $idx]],[[end]][[ $dc | quote ]][[ end ]] ]
  type = "service"

  group "transmission" {
    count = [[ .transmission.app_count ]]

    network {
      port "http" {
        to = "9091"
        [[- if gt (len .transmission.http_port_host_network) 0 ]]
        host_network = [[ .transmission.http_port_host_network | quote ]]
        [[- end ]]
      }

      port "downloads" {
        to = [[ .transmission.download_port.to | quote ]]
        [[- if gt (len .transmission.download_port.static) 0 ]]
        static = [[ .transmission.download_port.static | quote ]]
        [[- end ]]
        [[- if gt (len .transmission.download_port.host_network) 0 ]]
        host_network = [[ .transmission.download_port.host_network | quote ]]
        [[- end ]]
      }
    }

    service {
      port = "http"
      name = [[ .transmission.service.name | quote ]]
      [[- if gt (len .transmission.service.tags) 0 ]]
      tags = [
        [[- range $idx, $tag := .transmission.service.tags ]]
        [[ $tag | quote ]],
        [[- end ]]
      ]
      [[- end ]]

      check {
        type = "http"
        port = "http"
        path = "/"
        interval = "60s"
        timeout = "5s"
      }
    }

    [[- range $idx, $mount := .transmission.volumes ]]
    volume [[ $mount.name | quote ]] {
      type = [[ $mount.type | quote ]]
      source = [[ $mount.source | quote ]]
    }
    [[- end ]]

    task "transmission" {
      driver = "docker"

      config {
        image = "[[ .transmission.image ]]:[[ .transmission.image_tag ]]"
        ports = ["http", "downloads"]
        [[- if gt (len .transmission.docker_network) 0 ]]
        network_mode = [[ .transmission.docker_network | quote ]]
        [[- end ]]
        network_aliases = [
          "${NOMAD_TASK_NAME}",
        ]
      }

      resources {
        cpu = [[ .transmission.resources.cpu ]]
        memory = [[ .transmission.resources.memory ]]
      }

      env {
        TZ = [[ .transmission.timezone | quote ]]
        PUID = [[ .transmission.uid | quote ]]
        PGID = [[ .transmission.gid | quote ]]
      }

      [[- range $idx, $mount := .transmission.volumes ]]
      volume_mount {
        volume = [[ $mount.name | quote ]]
        destination = [[ $mount.destination | quote ]]
      }

      [[- end ]]
    }
  }
}
